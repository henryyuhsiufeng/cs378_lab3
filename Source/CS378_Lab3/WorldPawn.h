// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Components/BoxComponent.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "WorldPawn.generated.h"

class UBoxComponent;

UCLASS()
class CS378_LAB3_API AWorldPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AWorldPawn();

	/* The speed our ship moves around the level */
	UPROPERTY(Category = Gameplay, EditAnywhere, BlueprintReadWrite)
	float MoveSpeed;

	// Static names for axis bindings
	static const FName MoveForwardBinding;
	static const FName MoveRightBinding;
	static const FName InteractBinding;

	UFUNCTION(BlueprintCallable)
	void PerformInteraction();

	UFUNCTION(BlueprintImplementableEvent)
	void InteractPressed();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	/* The mesh component */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UStaticMeshComponent *PawnStaticMeshComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UBoxComponent *HitBoxComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UCameraComponent *CameraComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	USpringArmComponent *CameraBoom;

public:
	// Called every frame
	virtual void Tick(float DeltaSeconds) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent *PlayerInputComponent) override;

	/** Returns MeshComponent subobject **/
	FORCEINLINE class UStaticMeshComponent *GetMeshComponent() const { return PawnStaticMeshComponent; }
	/** Returns CameraComponent subobject **/
	FORCEINLINE class UCameraComponent *GetCameraComponent() const { return CameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent *GetCameraBoom() const { return CameraBoom; }
};
