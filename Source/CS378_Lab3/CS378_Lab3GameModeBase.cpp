// Copyright Epic Games, Inc. All Rights Reserved.

#include "CS378_Lab3GameModeBase.h"
#include "WorldPawn.h"

ACS378_Lab3GameModeBase::ACS378_Lab3GameModeBase()
{
    // set default pawn class to our character class
    static ConstructorHelpers::FObjectFinder<UClass> pawnBPClass(TEXT("Blueprint'/Game/Blueprints/MyWorldPawnBP.MyWorldPawnBP_C'"));

    if (GEngine)
    {
        if (pawnBPClass.Object)
        {
            GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, TEXT("PawnBP Found"));
            UClass *pawnBP = (UClass *)pawnBPClass.Object;
            DefaultPawnClass = pawnBP;
        }
        else
        {
            GEngine->AddOnScreenDebugMessage(-1, 1.f, FColor::Red, TEXT("PawnBP Not Found"));
            DefaultPawnClass = AWorldPawn::StaticClass();
        }
    }
}
