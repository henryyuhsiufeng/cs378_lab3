// Fill out your copyright notice in the Description page of Project Settings.

#include "ResponseActor.h"
#include "Components/StaticMeshComponent.h"
#include "TriggerActor.h"

// Sets default values
AResponseActor::AResponseActor()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
}

// Called when the game starts or when spawned
void AResponseActor::BeginPlay()
{
	Super::BeginPlay();
	if (tActor != NULL)
	{
		tActor->OnTriggerDelegate.AddDynamic(this, &AResponseActor::Response);
	}
}

// Called every frame
void AResponseActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AResponseActor::Response()
{
	// Calculate  movement
	FVector CurrentLoc = this->GetActorLocation();
	CurrentLoc.X += 50;
	SetActorLocation(CurrentLoc);
}